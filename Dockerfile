FROM openjdk:11

COPY ./build/libs/*.jar app.jar

COPY ./.docker/entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

EXPOSE 8081

ENTRYPOINT ./entrypoint.sh
