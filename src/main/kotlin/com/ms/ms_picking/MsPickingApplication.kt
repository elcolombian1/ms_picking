package com.ms.ms_picking

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.web.servlet.config.annotation.EnableWebMvc

@SpringBootApplication
@EnableJpaRepositories
@EnableWebMvc
@EnableAsync
class MsPickingApplication

fun main(args: Array<String>) {
    runApplication<MsPickingApplication>(*args)
}
