package com.ms.ms_picking.dto.orderPickedProductModification.v1

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

class OrderPickedProductModificationRequest {
    @JsonProperty("original_quantity")
    var originalQuantity: Int? = 0

    @JsonProperty("original_sub_total")
    var originalSubTotal: Double? = 0.0

    @JsonProperty("original_total")
    var originalTotal: Double? = 0.0

    @JsonProperty("original_total_tax")
    var originalTotalTax: Double? = 0.0

    @JsonProperty("final_quantity")
    var finalQuantity: Int? = 0

    @JsonProperty("final_sub_total")
    var finalSubTotal: Double? = 0.0

    @JsonProperty("final_total")
    var finalTotal: Double? = 0.0

    @JsonProperty("final_total_tax")
    var finalTotalTax: Double? = 0.0

    @JsonProperty("product_uuid")
    var productUuid: UUID? = null
}
