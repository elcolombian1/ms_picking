package com.ms.ms_picking.dto.orderPickedProductModification.v1

import com.ms.ms_picking.dto.BaseMapper
import com.ms.ms_picking.model.OrderPickedProductModification
import org.mapstruct.Mapper
import org.mapstruct.NullValueCheckStrategy
import org.mapstruct.NullValuePropertyMappingStrategy
import org.mapstruct.ReportingPolicy

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderPickedProductModificationMapper : BaseMapper<OrderPickedProductModification,
        OrderPickedProductModificationRequest,
        OrderPickedProductModificationDto>
