package com.ms.ms_picking.dto.orderPicked.v1

import com.ms.ms_picking.dto.BaseMapper
import com.ms.ms_picking.model.OrderPicked
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.NullValueCheckStrategy
import org.mapstruct.NullValuePropertyMappingStrategy
import org.mapstruct.ReportingPolicy

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderPickedMapper : BaseMapper<OrderPicked, OrderPickedRequest, OrderPickedDto> {
    @Mapping(target = "products", source = "model.orderPickedProducts")
    override fun modelToDto(model: OrderPicked): OrderPickedDto

    fun modelToDtoProducer(model: OrderPicked): OrderPickedDto
}
