package com.ms.ms_picking.dto.orderPicked.v1

import com.fasterxml.jackson.annotation.JsonProperty
import com.ms.ms_picking.dto.orderPickedProduct.v1.OrderPickedProductDto
import java.util.UUID

class OrderPickedRequest {
    @JsonProperty("order_uuid")
    var orderUuid: UUID? = null
    var status:String?=null
    var products: List<OrderPickedProductDto>? = null
}
