package com.ms.ms_picking.dto.orderPicked.v1

import com.ms.ms_catalog.dto.BaseDto
import com.ms.ms_picking.dto.orderPickedProduct.v1.OrderPickedProductDto
import java.util.UUID

class OrderPickedDto : BaseDto() {
    var orderUuid: UUID? = null
    var subTotal: Double? = 0.0
    var total: Double? = 0.0
    var totalTax: Double? = 0.0
    var status: String? = null
    var products: List<OrderPickedProductDto>? = null
}
