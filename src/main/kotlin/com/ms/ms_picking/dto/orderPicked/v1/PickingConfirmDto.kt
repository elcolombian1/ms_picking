package com.ms.ms_picking.dto.orderPicked.v1

import java.util.UUID

class PickingConfirmDto {
    var orderUuid: UUID? = null
}
