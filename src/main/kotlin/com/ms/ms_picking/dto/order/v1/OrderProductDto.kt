package com.ms.ms_picking.dto.order.v1

import java.util.UUID

class OrderProductDto {
    var subTotal: Double? = null
    var total: Double? = null
    var quantity: Int? = null
    var totalTax: Double? = null
    var productUuid: UUID? = null
}
