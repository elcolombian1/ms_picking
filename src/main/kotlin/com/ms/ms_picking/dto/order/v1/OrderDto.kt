package com.ms.ms_picking.dto.order.v1

import com.ms.ms_catalog.dto.BaseDto
import java.util.UUID

class OrderDto : BaseDto() {
    var clientUuid: UUID? = null
    var subTotal: Double? = null
    var total: Double? = null
    var totalTax: Double? = null
    var status: String? = null
    var orderProducts: List<OrderProductDto>? = null
}
