package com.ms.ms_picking.dto.orderPickedProduct.v1

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

class OrderPickedProductDto {
    var subTotal: Double? = 0.0
    var total: Double? = 0.0
    var quantity: Int? = 0
    var totalTax: Double? = 0.0

    @JsonProperty("product_uuid")
    var productUuid: UUID? = null
}
