package com.ms.ms_catalog.dto

import java.util.UUID

open class BaseDto {
    var uuid: UUID? = null
    var code: String? = null
}
