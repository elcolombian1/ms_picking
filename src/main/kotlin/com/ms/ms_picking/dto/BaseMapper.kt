package com.ms.ms_picking.dto

import org.mapstruct.MappingTarget

interface BaseMapper<Model, Request, Dto> {
    fun requestToModel(request: Request): Model
    fun modelToDto(model: Model): Dto
    fun update(request: Request, @MappingTarget model: Model)
}
