package com.ms.ms_picking.dto.orderDispatched

import com.ms.ms_catalog.dto.BaseDto
import java.util.UUID

class OrderDispatchedDto : BaseDto() {
    var orderUuid: UUID? = null
    var subTotal: Double? = null
    var total: Double? = null
    var totalTax: Double? = null
    var status: String? = null
    var products: List<OrderDispatchedProductDto>? = null
}
