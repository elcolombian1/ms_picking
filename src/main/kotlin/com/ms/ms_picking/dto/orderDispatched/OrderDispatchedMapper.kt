package com.ms.ms_picking.dto.orderDispatched

import com.ms.ms_picking.dto.BaseMapper
import com.ms.ms_picking.model.OrderDispatched
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.NullValueCheckStrategy
import org.mapstruct.NullValuePropertyMappingStrategy
import org.mapstruct.ReportingPolicy

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderDispatchedMapper : BaseMapper<OrderDispatched, OrderDispatchedRequest, OrderDispatchedDto> {
    override fun modelToDto(model: OrderDispatched): OrderDispatchedDto
    fun modelToDtoProducer(model: OrderDispatched): OrderDispatchedDto
}
