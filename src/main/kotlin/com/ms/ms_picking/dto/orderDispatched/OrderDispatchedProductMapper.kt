package com.ms.ms_picking.dto.orderDispatched

import com.ms.ms_picking.dto.BaseMapper
import com.ms.ms_picking.model.OrderDispatchedProduct
import com.ms.ms_picking.model.OrderPickedProduct
import org.mapstruct.Mapper
import org.mapstruct.NullValueCheckStrategy
import org.mapstruct.NullValuePropertyMappingStrategy
import org.mapstruct.ReportingPolicy

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderDispatchedProductMapper : BaseMapper<OrderDispatchedProduct, OrderDispatchedProductRequest, OrderDispatchedProductDto>
