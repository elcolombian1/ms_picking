package com.ms.ms_picking.dto.orderDispatched

import com.ms.ms_picking.model.OrderDispatched
import java.util.*

class OrderDispatchedProductDto {
    var orderDispatched: OrderDispatchedDto? = null
    var subTotal: Double? = null
    var total: Double? = null
    var quantity: Int? = null
    var totalTax: Double? = null
    var productUuid: UUID? = null
}
