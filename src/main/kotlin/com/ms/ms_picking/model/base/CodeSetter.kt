package com.ms.ms_user.model.base

import com.ms.ms_picking.repository.baseRepository.BaseRepository

abstract class CodeSetter {
    fun setTulCode(repository: BaseRepository, baseModel: BaseModel, prefix: String? = null) {
        if (baseModel.code == null) {
            val maxTulCode =
                repository.findFirstByCodeNotNullOrderByCodeDesc()?.code?.substring(3)?.toLong() ?: 0
            baseModel.code = baseModel.getCode(maxTulCode + 1, prefix)
        }
    }
}
