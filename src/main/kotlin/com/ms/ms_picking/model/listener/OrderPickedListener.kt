package com.ms.ms_picking.model.listener

import com.ms.ms_picking.kafka.producer.OrderPickedProducer
import com.ms.ms_picking.model.OrderPicked
import com.ms.ms_picking.model.OrderPickedProduct
import com.ms.ms_picking.repository.orderPickedRepository.OrderPickedRepository
import com.ms.ms_user.model.base.CodeSetter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class OrderPickedListener : CodeSetter() {
    companion object {
        lateinit var orderPickedRepository: OrderPickedRepository
                lateinit var orderPickedProducer: OrderPickedProducer
    }

    @Autowired
    fun setCompanion(
        _orderPickedRepository: OrderPickedRepository,
        _orderPickedProducer:OrderPickedProducer
    ) {
        orderPickedRepository = _orderPickedRepository
        orderPickedProducer=_orderPickedProducer
    }


    @PrePersist
    fun prePersist(OrderPicked: OrderPicked) {
        this.setTulCode(orderPickedRepository, OrderPicked)
    }
    @PostPersist
    fun postPersist(orderPicked: OrderPicked) {
        this.setTulCode(orderPickedRepository, orderPicked)
        orderPickedProducer.confirmOrderPicking(orderPicked)
    }
}
