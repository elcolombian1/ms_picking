package com.ms.ms_picking.model.listener

import com.ms.ms_picking.model.OrderPickedProduct
import com.ms.ms_picking.repository.orderPickedProductModificationRepository.OrderPickedProductModificationRepository
import com.ms.ms_user.model.base.CodeSetter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.PrePersist

@Component
class OrderPickedProductModificationListener : CodeSetter() {
    companion object {
        lateinit var orderPickedProductModificationRepository: OrderPickedProductModificationRepository
    }

    @Autowired
    fun setCompanion(
        _orderPickedProductModificationRepository: OrderPickedProductModificationRepository
    ) {
        orderPickedProductModificationRepository = _orderPickedProductModificationRepository
    }

    private val log = LoggerFactory.getLogger(javaClass)

    @PrePersist
    fun prePersist(orderPickedProductModification: OrderPickedProduct) {
        this.setTulCode(orderPickedProductModificationRepository, orderPickedProductModification)
    }
}
