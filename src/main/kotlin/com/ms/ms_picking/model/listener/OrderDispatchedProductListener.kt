package com.ms.ms_picking.model.listener

import com.ms.ms_picking.model.OrderDispatchedProduct
import com.ms.ms_picking.model.OrderPickedProduct
import com.ms.ms_picking.repository.orderDispatchedProductRepository.OrderDispatchedProductRepository
import com.ms.ms_user.model.base.CodeSetter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class OrderDispatchedProductListener : CodeSetter() {
    companion object {
        lateinit var orderPickedProductRepository: OrderDispatchedProductRepository
    }

    @Autowired
    fun setCompanion(
        _orderPickedProductRepository: OrderDispatchedProductRepository
    ) {
        orderPickedProductRepository = _orderPickedProductRepository
    }

    @PrePersist
    fun prePersist(orderDispatchedProduct: OrderDispatchedProduct) {
        this.setTulCode(orderPickedProductRepository, orderDispatchedProduct)
    }

}
