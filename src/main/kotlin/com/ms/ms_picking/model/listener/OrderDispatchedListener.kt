package com.ms.ms_picking.model.listener

import com.ms.ms_picking.kafka.producer.OrderPickedProducer
import com.ms.ms_picking.model.OrderDispatched
import com.ms.ms_picking.repository.orderDispatchedRepository.OrderDispatchedRepository
import com.ms.ms_user.model.base.CodeSetter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class OrderDispatchedListener : CodeSetter() {
    companion object {
        lateinit var orderDispatchedRepository: OrderDispatchedRepository
        lateinit var orderDispatchedProducer: OrderPickedProducer
    }

    @Autowired
    fun setCompanion(
        _orderDispatchedRepository: OrderDispatchedRepository,
        _orderDispatchedProducer:OrderPickedProducer

    ) {
        orderDispatchedRepository = _orderDispatchedRepository
        orderDispatchedProducer =_orderDispatchedProducer

    }


    @PrePersist
    fun prePersist(OrderDispatched: OrderDispatched) {
        this.setTulCode(orderDispatchedRepository, OrderDispatched)
    }
    @PostPersist
    fun postPersist(orderDispatched: OrderDispatched) {
        this.setTulCode(orderDispatchedRepository, orderDispatched)
        orderDispatchedProducer.confirmOrderDispatch(orderDispatched)
    }
}
