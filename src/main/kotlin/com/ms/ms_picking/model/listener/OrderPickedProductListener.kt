package com.ms.ms_picking.model.listener

import com.ms.ms_picking.model.OrderPickedProduct
import com.ms.ms_picking.repository.orderDispatchedProductRepository.OrderDispatchedProductRepository
import com.ms.ms_user.model.base.CodeSetter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.PrePersist

@Component
class OrderPickedProductListener : CodeSetter() {
    companion object {
        lateinit var orderPickedProductRepository: OrderDispatchedProductRepository
    }

    @Autowired
    fun setCompanion(
        _orderPickedProductRepository: OrderDispatchedProductRepository
    ) {
        orderPickedProductRepository = _orderPickedProductRepository
    }

    private val log = LoggerFactory.getLogger(javaClass)

    @PrePersist
    fun prePersist(orderPickedProduct: OrderPickedProduct) {
        this.setTulCode(orderPickedProductRepository, orderPickedProduct)
    }
}
