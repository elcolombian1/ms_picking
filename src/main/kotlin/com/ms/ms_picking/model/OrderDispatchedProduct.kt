package com.ms.ms_picking.model

import com.ms.ms_picking.model.listener.OrderDispatchedProductListener
import com.ms.ms_user.model.base.BaseModel
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@EntityListeners(
    value = [
        OrderDispatchedProductListener::class,
        AuditingEntityListener::class,
    ]
)

@Table
data class OrderDispatchedProduct(
    @ManyToOne(fetch = FetchType.LAZY)
    var orderDispatched: OrderDispatched? = null,
    var subTotal: Double? = 0.0,
    var total: Double? = 0.0,
    var quantity: Int? = 0,
    var totalTax: Double? = 0.0,
    var productUuid: UUID? = null
) : BaseModel()
