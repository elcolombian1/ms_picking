package com.ms.ms_picking.model

import com.ms.ms_picking.model.listener.OrderDispatchedListener
import com.ms.ms_picking.model.listener.OrderPickedListener
import com.ms.ms_user.model.base.BaseModel
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.FetchType
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@EntityListeners(
    value = [
        OrderPickedListener::class,
        AuditingEntityListener::class,
    ]
)

@Table(name = "order_picked")
data class OrderPicked(
    var orderUuid: UUID? = null,
    var subTotal: Double? = 0.0,
    var total: Double? = 0.0,
    var totalTax: Double? = 0.0,
    var status: String? = null,
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "orderPicked")
    var orderPickedProducts: List<OrderPickedProduct>? = null
) : BaseModel()
