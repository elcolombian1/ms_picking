package com.ms.ms_picking.model

import com.ms.ms_picking.model.listener.OrderPickedProductListener
import com.ms.ms_user.model.base.BaseModel
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.FetchType
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@EntityListeners(
    value = [
        OrderPickedProductListener::class,
        AuditingEntityListener::class,
    ]
)

@Table(name = "order_picked_product_modifications")
data class OrderPickedProductModification(
    @ManyToOne(fetch = FetchType.LAZY)
    var orderPickedProduct: OrderPickedProduct,
    var originalQuantity: Int? = 0,
    var originalSubTotal: Double? = 0.0,
    var originalTotal: Double? = 0.0,
    var originalTotalTax: Double? = 0.0,
    var finalQuantity: Int? = 0,
    var finalSubTotal: Double? = 0.0,
    var finalTotal: Double? = 0.0,
    var finalTotalTax: Double? = 0.0,
    var productUuid: UUID? = null
) : BaseModel()
