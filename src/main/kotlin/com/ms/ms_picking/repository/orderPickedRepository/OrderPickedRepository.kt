package com.ms.ms_picking.repository.orderPickedRepository

import com.ms.ms_picking.repository.baseRepository.BaseRepository
import com.ms.ms_picking.model.OrderPicked
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderPickedRepository :
    JpaRepository<OrderPicked, UUID>,
    JpaSpecificationExecutor<OrderPicked>,
    BaseRepository {
    fun findAllByStatus(status: String): List<OrderPicked>
}
