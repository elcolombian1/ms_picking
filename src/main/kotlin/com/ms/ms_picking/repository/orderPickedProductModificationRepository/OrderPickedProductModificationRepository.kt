package com.ms.ms_picking.repository.orderPickedProductModificationRepository

import com.ms.ms_picking.repository.baseRepository.BaseRepository
import com.ms.ms_picking.model.OrderPickedProductModification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderPickedProductModificationRepository :
    JpaRepository<OrderPickedProductModification, UUID>,
    JpaSpecificationExecutor<OrderPickedProductModification>,
    BaseRepository
