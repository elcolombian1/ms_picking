package com.ms.ms_picking.repository.baseRepository

import com.ms.ms_user.model.base.BaseModel

interface BaseRepository {
    fun findFirstByCodeNotNullOrderByCodeDesc(): BaseModel?
}
