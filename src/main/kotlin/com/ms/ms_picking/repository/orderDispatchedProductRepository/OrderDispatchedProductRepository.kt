package com.ms.ms_picking.repository.orderDispatchedProductRepository

import com.ms.ms_picking.repository.baseRepository.BaseRepository
import com.ms.ms_picking.model.OrderDispatchedProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderDispatchedProductRepository :
    JpaRepository<OrderDispatchedProduct, UUID>,
    JpaSpecificationExecutor<OrderDispatchedProduct>,
    BaseRepository {
    fun findByOrderDispatchedUuid(orderPickedUUID: UUID?): List<OrderDispatchedProduct>
}
