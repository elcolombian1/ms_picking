package com.ms.ms_picking.repository.orderPickedProductRepository

import com.ms.ms_picking.repository.baseRepository.BaseRepository
import com.ms.ms_picking.model.OrderPickedProduct
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderPickedProductRepository :
    JpaRepository<OrderPickedProduct, UUID>,
    JpaSpecificationExecutor<OrderPickedProduct>,
    BaseRepository {
    fun findByOrderPickedUuid(orderPickedUUID: UUID?): List<OrderPickedProduct>
}
