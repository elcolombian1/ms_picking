package com.ms.ms_picking.repository.orderDispatchedRepository

import com.ms.ms_picking.repository.baseRepository.BaseRepository
import com.ms.ms_picking.model.OrderDispatched
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderDispatchedRepository :
    JpaRepository<OrderDispatched, UUID>,
    JpaSpecificationExecutor<OrderDispatched>,
    BaseRepository {
    fun findAllByStatus(status: String): List<OrderDispatched>
}
