package com.ms.ms_picking.service.crud

import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedRequest
import com.ms.ms_picking.model.OrderPicked
import org.springframework.data.jpa.domain.Specification
import java.util.UUID

interface CrudService<Dto, Request> {
    fun findAll(spec: Specification<OrderPicked>?): List<Dto>
    fun getOne(uuid: UUID): Dto
    fun create(request: Request): Dto
    fun update(uuid: UUID, request: Request): Dto
    fun delete(uuid: UUID)
    fun confirmDispatch(orderPickedRequest: OrderPickedRequest)
}
