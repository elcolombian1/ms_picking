package com.ms.ms_picking.service.orderPicked.v1

import com.ms.ms_picking.service.crud.CrudService
import com.ms.ms_picking.dto.order.v1.OrderDto
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedDto
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedRequest
import com.ms.ms_picking.model.OrderPicked

interface OrderPickedService : CrudService<OrderPickedDto, OrderPickedRequest> {
    fun savePendingPicked(orderDto: OrderDto)
    fun indexPending(): List<OrderPickedDto>
    fun createModel(request: OrderPickedRequest): OrderPicked
}
