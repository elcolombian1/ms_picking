package com.ms.ms_picking.service.orderPicked.v1.impl

import com.ms.ms_picking.dto.order.v1.OrderDto
import com.ms.ms_picking.dto.orderDispatched.OrderDispatchedProductMapper
import com.ms.ms_picking.dto.orderDispatched.OrderDispatchedProductRequest
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedDto
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedMapper
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedRequest
import com.ms.ms_picking.dto.orderPickedProduct.v1.OrderPickedProductMapper
import com.ms.ms_picking.dto.orderPickedProduct.v1.OrderPickedProductRequest
import com.ms.ms_picking.model.OrderDispatched
import com.ms.ms_picking.model.OrderDispatchedProduct
import com.ms.ms_picking.model.OrderPicked
import com.ms.ms_picking.repository.orderDispatchedProductRepository.OrderDispatchedProductRepository
import com.ms.ms_picking.repository.orderDispatchedRepository.OrderDispatchedRepository
import com.ms.ms_picking.repository.orderPickedProductRepository.OrderPickedProductRepository
import com.ms.ms_picking.repository.orderPickedRepository.OrderPickedRepository
import com.ms.ms_picking.service.orderPicked.v1.OrderPickedService
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.UUID

@Service
class OrderPickedServiceImpl(
    private val orderPickedRepository: OrderPickedRepository,
    private val orderPickedProductRepository: OrderPickedProductRepository,
    private val orderPickedProductMapper: OrderPickedProductMapper,
    private val orderPickedMapper: OrderPickedMapper,
    private val orderDispatchedRepository: OrderDispatchedRepository,
    private val orderDispatchedProductRepository: OrderDispatchedProductRepository,
    private val orderDispatchedProductMapper: OrderDispatchedProductMapper
) : OrderPickedService {
    override fun savePendingPicked(orderDto: OrderDto) {
        val orderPicked = createModel(
            OrderPickedRequest().apply {
                this.orderUuid = orderDto.uuid
            }
        )

        orderDto.orderProducts?.forEach { orderProduct ->
            val orderProductRequest = OrderPickedProductRequest().apply {
                this.productUuid = orderProduct.productUuid
                this.quantity = orderProduct.quantity
                this.subTotal = orderProduct.subTotal
                this.total = orderProduct.total
                this.totalTax = orderProduct.totalTax
            }
            val orderProductModel = orderProductRequest.let(orderPickedProductMapper::requestToModel)
            orderProductModel.orderPicked = orderPicked
            orderPickedProductRepository.save(orderProductModel)
        }
    }

    override fun indexPending(): List<OrderPickedDto> {
        return orderPickedRepository.findAllByStatus("pending").map(orderPickedMapper::modelToDto)
    }

    override fun findAll(spec: Specification<OrderPicked>?): List<OrderPickedDto> {
        return orderPickedRepository.findAll(spec).map(orderPickedMapper::modelToDto)
    }

    override fun getOne(uuid: UUID): OrderPickedDto {
        return orderPickedRepository.findById(uuid).orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
            .let(orderPickedMapper::modelToDto)
    }

    override fun createModel(request: OrderPickedRequest): OrderPicked {
        return orderPickedRepository.save(orderPickedMapper.requestToModel(request))
    }

    @Transactional
    override fun create(request: OrderPickedRequest): OrderPickedDto {
        val orderPicked = orderPickedRepository.save(orderPickedMapper.requestToModel(request))
        request.products?.forEach { product ->
            val orderProductRequest = OrderPickedProductRequest().apply {
                this.productUuid = product.productUuid
                this.quantity = product.quantity
            }
            val orderProductModel = orderProductRequest.let(orderPickedProductMapper::requestToModel)
            orderProductModel.orderPicked = orderPicked
            orderPickedProductRepository.save(orderProductModel)
        }
        return orderPicked.let(orderPickedMapper::modelToDto)
    }

    override fun update(uuid: UUID, request: OrderPickedRequest): OrderPickedDto {
        val orderPicked =
            orderPickedRepository.findById(uuid).orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) }
        orderPickedMapper.update(request, orderPicked)
        return orderPickedRepository.save(orderPicked).let(orderPickedMapper::modelToDto)
    }

    override fun delete(uuid: UUID) {
        if (orderPickedRepository.existsById(uuid)) {
            orderPickedRepository.deleteById(uuid)
        }
    }

    @Transactional
    override fun confirmDispatch(orderPickedRequest: OrderPickedRequest) {
        val orderPicked =
            orderPickedRequest.orderUuid?.let { orderPickedRepository.findById(it).orElseThrow { ResponseStatusException(HttpStatus.NOT_FOUND) } }
        createDispatchedOrder(orderPickedRequest)
        orderPicked?.status = "completed"
        orderPicked?.let { orderPickedRepository.save(it) }
    }

    private fun createDispatchedOrder(orderPickedRequest: OrderPickedRequest) {
        val orderDispatched = OrderDispatched().apply {
            this.orderUuid = orderPickedRequest.orderUuid?.let { orderPickedRepository.findById(it).get() }?.orderUuid
        }
       val orderDispatchedSaved= orderDispatchedRepository.save(orderDispatched)
        orderPickedRequest.products?.forEach { product ->
            val orderDispatchedProductRequest = OrderDispatchedProductRequest().apply {
                this.productUuid = product.productUuid
                this.quantity = product.quantity
            }

            val orderDispatchedProduct =orderDispatchedProductRequest.let(orderDispatchedProductMapper::requestToModel)
            orderDispatchedProduct.orderDispatched=orderDispatchedSaved
            orderDispatchedProductRepository.save(orderDispatchedProduct)
        }
    }
}
