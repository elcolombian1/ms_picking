package com.ms.ms_picking.service.orderConsumerService

import com.fasterxml.jackson.databind.ObjectMapper
import com.ms.ms_picking.dto.order.v1.OrderDto
import com.ms.ms_picking.kafka.factory.KafkaActionCatalog
import com.ms.ms_picking.kafka.factory.KafkaProcessorService
import com.ms.ms_picking.kafka.factory.KafkaTopicCatalog
import com.ms.ms_picking.service.orderPicked.v1.OrderPickedService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class OrderKafkaConsumer(
    private val objectMapper: ObjectMapper,
    private val orderPickedService: OrderPickedService
) : KafkaProcessorService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAction() = KafkaActionCatalog.ORDER_CREATED

    override fun getTopic() = KafkaTopicCatalog.MS_ORDERS_ORDER

    override fun <T> process(request: T) {

        process(objectMapper.convertValue(request, OrderDto::class.java))
    }

    private fun process(orderDto: OrderDto) {
        orderPickedService.savePendingPicked(orderDto)
    }
}
