package com.ms.ms_picking.controller.orderPicked.v1

import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedDto
import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedRequest
import com.ms.ms_picking.model.OrderPicked
import com.ms.ms_picking.service.orderPicked.v1.OrderPickedService
import com.sipios.springsearch.anotation.SearchSpec
import com.tul.logistic.logistic_auditul.dto.request.v1.OnCreate
import org.springframework.cache.annotation.CacheConfig
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController("order-picked.v1.crud")
@RequestMapping("v1/order-picked")
@Validated
@CacheConfig(cacheNames = ["v1/order-picked"])
class OrderPickedController(
    private val orderPickedService: OrderPickedService
) {
    @GetMapping("pending")
    fun indexPending(): ResponseEntity<List<OrderPickedDto>> {
        return ResponseEntity(orderPickedService.indexPending(), HttpStatus.OK)
    }

    @GetMapping
    fun index(@SearchSpec(caseSensitiveFlag = false) specs: Specification<OrderPicked>?): ResponseEntity<List<OrderPickedDto>> {
        return ResponseEntity(orderPickedService.findAll(specs), HttpStatus.OK)
    }

    @GetMapping("{orderPickedUuid}")
    fun show(@PathVariable orderPickedUuid: UUID): ResponseEntity<OrderPickedDto> {
        return ResponseEntity(orderPickedService.getOne(orderPickedUuid), HttpStatus.OK)
    }

    @PostMapping
    fun create(@Validated(OnCreate::class) @RequestBody request: OrderPickedRequest): ResponseEntity<OrderPickedDto> {
        return ResponseEntity(orderPickedService.create(request), HttpStatus.OK)
    }

    @PatchMapping("{orderPickedUuid}")
    fun update(
        @RequestBody request: OrderPickedRequest,
        @PathVariable orderPickedUuid: UUID
    ): ResponseEntity<OrderPickedDto> {
        return ResponseEntity(orderPickedService.update(orderPickedUuid, request), HttpStatus.OK)
    }

    @DeleteMapping("{orderPickedUuid}")
    fun delete(@PathVariable orderPickedUuid: UUID) {
        orderPickedService.delete(orderPickedUuid)
    }

    @PostMapping("confirm-dispatch")
    fun confirmDispatch(@RequestBody orderPickedRequest: OrderPickedRequest) {
        orderPickedService.confirmDispatch(orderPickedRequest)
    }
}
