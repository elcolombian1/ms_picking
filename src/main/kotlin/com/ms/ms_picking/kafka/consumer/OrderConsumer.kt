package com.ms.ms_picking.kafka.consumer

import com.ms.ms_picking.kafka.factory.KafkaMessage
import com.ms.ms_picking.kafka.factory.KafkaProcessorFactory
import com.ms.ms_picking.kafka.factory.KafkaTopicCatalog
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class OrderConsumer(
    private val processorFactory: KafkaProcessorFactory
) {
    val log: Logger = LoggerFactory.getLogger(javaClass)

    @KafkaListener(
        topics = ["ms_orders_order"],
        containerFactory = "config.kafka.consumerFactory"
    )
    fun regularPackageUpdated(message: KafkaMessage) {
        processorFactory.getProcessor(KafkaTopicCatalog.MS_ORDERS_ORDER, message.action)
            ?.process(message.data)
    }
}
