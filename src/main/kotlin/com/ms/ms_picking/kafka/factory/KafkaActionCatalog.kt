package com.ms.ms_picking.kafka.factory

enum class KafkaActionCatalog {
    ORDER_CREATED,
    CONFIRM_PICKING,
    CONFIRM_DISPATCH,
    ORDER_PROCESSED;

    companion object {

        private val actions = HashMap<String, KafkaActionCatalog>()

        fun getByName(name: String?): KafkaActionCatalog? {
            return actions.filter {
                it.value.name == name?.uppercase()
            }.values.firstOrNull()
        }

        init {
            values().forEach { country ->
                actions[country.name] = country
            }
        }
    }
}
