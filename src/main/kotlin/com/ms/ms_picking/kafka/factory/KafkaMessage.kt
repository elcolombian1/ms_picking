package com.ms.ms_picking.kafka.factory

open class KafkaMessage {
    var action: String? = null
    var country: String? = null
    var data: Any? = null
}
