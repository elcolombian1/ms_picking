package com.ms.ms_picking.kafka.factory

import org.springframework.stereotype.Component

@Component
class KafkaProcessorFactory(
    processorServices: Set<KafkaProcessorService>
) {

    private val kafkaProcessorDiscover: MutableMap<Pair<KafkaTopicCatalog, KafkaActionCatalog>, KafkaProcessorService> =
        mutableMapOf()

    fun getProcessor(kafkaTopicCatalog: KafkaTopicCatalog, operationName: String?): KafkaProcessorService? {
        return kafkaProcessorDiscover[Pair(kafkaTopicCatalog, KafkaActionCatalog.getByName(operationName))]
    }

    init {
        processorServices.forEach { processor ->
            kafkaProcessorDiscover[Pair(processor.getTopic(), processor.getAction())] = processor
        }
    }
}
