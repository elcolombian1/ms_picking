package com.ms.ms_picking.kafka.factory

enum class KafkaTopicCatalog {
    MS_ORDERS_ORDER,
    MS_PICKING_ORDER_PICKED,
    MS_PICKING_PICKING
}
