package com.ms.ms_picking.kafka.factory

interface KafkaProcessorService {

    fun getAction(): KafkaActionCatalog
    fun getTopic(): KafkaTopicCatalog
    fun <T> process(request: T)
}
