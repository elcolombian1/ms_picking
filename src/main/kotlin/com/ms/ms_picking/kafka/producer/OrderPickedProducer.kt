package com.ms.ms_picking.kafka.producer

import com.ms.ms_picking.dto.orderPicked.v1.OrderPickedMapper
import com.ms.ms_picking.dto.orderPicked.v1.PickingConfirmDto
import com.ms.ms_picking.dto.orderPickedProduct.v1.OrderPickedProductMapper
import com.ms.ms_picking.kafka.factory.KafkaActionCatalog
import com.ms.ms_picking.kafka.factory.KafkaDispatcher
import com.ms.ms_picking.kafka.factory.KafkaTopicCatalog
import com.ms.ms_picking.model.OrderDispatched
import com.ms.ms_picking.model.OrderPicked
import com.ms.ms_picking.repository.orderPickedProductRepository.OrderPickedProductRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class OrderPickedProducer(
    private val kafkaDispatcher: KafkaDispatcher,
    private val orderPickedMapper: OrderPickedMapper,
    private val orderPickedProductMapper: OrderPickedProductMapper,
    private val orderPickedProductRepository: OrderPickedProductRepository,

) {
    @Async
    fun updated(order: OrderPicked) {
        if (order.status != "completed") {
            return
        }
        val data = orderPickedMapper.modelToDtoProducer(order)
        data.products = orderPickedProductRepository.findByOrderPickedUuid(order.uuid)
            .map(orderPickedProductMapper::modelToDto)
        print(KafkaTopicCatalog.MS_PICKING_ORDER_PICKED.name.lowercase())
        this.kafkaDispatcher.sendMessage(
            data = data,
            topic = KafkaTopicCatalog.MS_PICKING_ORDER_PICKED.name.lowercase(),
            action = KafkaActionCatalog.ORDER_PROCESSED.name.lowercase()
        )
    }

    @Async
    fun confirmOrderPicking(order: OrderPicked) {
        this.kafkaDispatcher.sendMessage(
            data = PickingConfirmDto().apply { this.orderUuid = order.orderUuid },
            topic = KafkaTopicCatalog.MS_PICKING_PICKING.name.lowercase(),
            action = KafkaActionCatalog.CONFIRM_PICKING.name.lowercase()
        )
    }

    @Async
    fun confirmOrderDispatch(order: OrderDispatched) {
        this.kafkaDispatcher.sendMessage(
            data = PickingConfirmDto().apply { this.orderUuid = order.orderUuid },
            topic = KafkaTopicCatalog.MS_PICKING_PICKING.name.lowercase(),
            action = KafkaActionCatalog.CONFIRM_DISPATCH.name.lowercase()
        )
    }
}
